import json
from datetime import datetime

class GalleryDb(object):
    def __init__(self, path):
        self.path = path
        try:
            with open(self.path) as fh:
                self.json = json.load(fh)
            print 'Gallery db loaded from %s' % self.path
        except:
            with open(self.path, 'w') as fh:
                fh.write('{"galleries": {}}')
            with open(self.path) as fh:
                self.json = json.load(fh)
            print 'File does not exist but was created with empty galleries'

    def get_gallery(self, gallery_name):
        if gallery_name not in self.json['galleries']:
            print 'Gallery %s does not exist' % gallery_name
            return {}
        else:
            print 'Gallery %s exists' % gallery_name
            return self.json['galleries'][gallery_name]

    def create_gallery(self, name='gallery_name', paths=None):
        if paths == None:
            paths = []

        if name in self.json['galleries']:
            print 'Gallery %s already exists - will be overwritten' % name
            date_col = 'modified'
        else:
            print 'New gallery %s will be created' % name
            date_col = 'added'

        self.json['galleries'][name] = { 'paths': paths }
        self.json['galleries'][name][date_col] = datetime.now().strftime('%Y-%m-%d  %H:%M:%S')

        self.commit()

    def commit(self):
        with open(self.path, 'w') as fh:
            json.dump(self.json, fh)

def load_from_json(path):
    return GalleryDb(path)