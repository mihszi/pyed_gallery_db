import sys

def _parse_line(line):
    """
    >>> _parse_line('ana are')
    (['ana', 'are'], {})
    >>> _parse_line('ana --are 4')
    (['ana'], {'are': '4'})
    >>> _parse_line('--are 4 ana')
    Traceback (most recent call last):
    ...
    ValueError: positional arg after keyword arg
    >>> _parse_line('--@ 3')
    Traceback (most recent call last):
    ...
    ValueError: keyword arg must start with letter
    >>> _parse_line('ana --are')
    Traceback (most recent call last):
    ...
    ValueError: missing keyword arg value
    """

    # try:
    args = line.split()
    l = []
    dt = {}
    i = 0
    karg = False
    while i < len(args):
        if args[i][0:2] == '--':
            if not args[i][2:3].isalpha():
                raise ValueError('keyword arg must start with letter')

            if i+1 < len(args):
                karg = True
                dt[args[i][2:]] = args[i+1]
                i += 2
            else:
                raise ValueError('missing keyword arg value')
        elif args[i][0:2] != '--':
            if karg:
                raise ValueError('positional arg after keyword arg')
            l.append(args[i])
            i += 1

    return (l, dt)

    # except ValueError as e:
    #     print 'ValueError: ' + str(e)




#_parse_line('--aasfasfa')

#_parse_line('ana are')
#_parse_line('ana --are 4')
# _parse_line('--are 4 ana')
# _parse_line('--34 3')
_parse_line('--a 3')

#_parse_line('ana --are')


def ana(*args, **kwargs):
    for x in args:
        print x, ' from ana list'

    for k in kwargs:
        print kwargs[k], ' from ana dict'

_registry = {'ana': ana}

def main():
    cmd_args = sys.argv
    cmd_line = ' '.join(cmd_args[1:])
    f_arg, f_d  = _parse_line(cmd_line)
    if f_arg:
        func_name = f_arg[0]
        _registry[func_name](*f_arg[1:], **f_d)
    else:
        raise Exception('no args or no pos args')


def command(func):
    _registry[func.__name__] = func
    return func

#main()