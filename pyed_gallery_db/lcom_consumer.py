from lcom import main, command
from contextlib import contextmanager
from gallery_json_db import load_from_json
import os, os.path
import uuid
import shutil
import traceback
import pprint
import sqlite3

@contextmanager
def _lock_file(lock_path):
    '''context for creating and deleting a lock file'''
    if os.path.exists(lock_path):
        raise IOError('lock file exists')
    else:
        open(lock_path, 'w').close()
    yield
    os.remove(lock_path)

def _image_store(images):
    ''' function for creating a guid for a file and move it to it's specific directory'''
    processed_images = []
    if not images:
        # no path given
        print "no paths provided"
    else:
        for image in images:
            if not os.path.exists(image):
                #path inexistent
                print 'provided path %s does not exist' % image
            else:
                #generate uuid
                file_uuid = uuid.uuid4()
                dir = '/home/rares_dr/work/python/training/module1-exercises-rares-fork/day2/gallery_dir_output/' + str(file_uuid)[:1]
                proc_path = '/'.join([dir, str(file_uuid)])

                if not os.path.exists(dir):
                    os.makedirs(dir)

                shutil.copy(image, proc_path)
                processed_images.append(proc_path)
                print '%s: %s' % (image, str(file_uuid))

        print
        return processed_images

def _get_imgs_from_path(dir_path, ext='jpg'):
    '''function for parsing a dir recursively
    and returning all files with a specific extensions'''
    all_imgs = []
    for cur_path, dirs, files in os.walk(dir_path):
        all_imgs += ['/'.join([cur_path, file]) for file in files if file.endswith('.%s' % ext)]

    return all_imgs
    #pprint.pprint(all_imgs, width=1)

@command
def add_gallery(gallery_name, gallery_path, **kwargs):
    imgs_to_process = _get_imgs_from_path(gallery_path, **kwargs)
    if not imgs_to_process:
        print 'no imgs found in %s' % gallery_path
        return
    else:
        print 'Images found in the input dir %s: ' % gallery_path
        pprint.pprint(imgs_to_process, depth=5)
        print

    processed_imgs = _image_store(imgs_to_process)

    gallery_db = load_from_json('gallery.db')
    gallery_db.create_gallery(name=gallery_name, paths=processed_imgs)

@command
def show_gallery(gallery_name):
    gallery_db = load_from_json('gallery.db')
    pprint.pprint(gallery_db.get_gallery(gallery_name), depth = 5)

with _lock_file('db.lock'):
    try:
        main()
    except Exception as e:
        print 'main function raised exception: '
        traceback.print_exc()